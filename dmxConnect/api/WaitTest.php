<?php
require('../../dmxConnectLib/dmxConnect.php');


$app = new \lib\App();

$app->define(<<<'JSON'
{
  "meta": {
    "$_GET": [
      {
        "type": "number",
        "options": {
          "rules": {
            "core:required": {}
          }
        },
        "name": "time"
      }
    ]
  },
  "exec": {
    "steps": [
      {
        "name": "begin",
        "module": "core",
        "action": "setvalue",
        "options": {
          "key": "begin",
          "value": "{{NOW}}"
        },
        "output": true
      },
      {
        "name": "sleep",
        "module": "WaitModule",
        "action": "Wait",
        "options": {
          "value": "{{$_GET.time}}"
        }
      },
      {
        "name": "end",
        "module": "core",
        "action": "setvalue",
        "options": {
          "key": "end",
          "value": "{{NOW}}"
        },
        "output": true
      }
    ]
  }
}
JSON
);
?>