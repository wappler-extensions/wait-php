<?php

namespace modules;

use \lib\core\Module;

class WaitModule extends Module
{
  public function Wait($options, $name) {
    //Sleep time in ms
    usleep($this->app->parseObject($options->value) * 1000);
    return true;
  }
}

?>